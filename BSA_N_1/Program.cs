﻿using System;
using System.Collections.Generic;
using BSA_N_1.Models;
using Newtonsoft.Json;

namespace BSA_N_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var processor = new DataProcessor(new HttpDataGetter(new Dictionary<Route, string>
            {
                {Route.AllProjects, "https://bsa21.azurewebsites.net/api/Projects"},
                {Route.AllTasks, "https://bsa21.azurewebsites.net/api/Tasks"},
                {Route.AllTeams, "https://bsa21.azurewebsites.net/api/Teams"},
                {Route.AllUsers, "https://bsa21.azurewebsites.net/api/Users"},
            })
                .GetData());
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("First request: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Enter value of UserId");
            int buffer = Convert.ToInt32(Console.ReadLine());
            Console.ForegroundColor = default;
            foreach ((var key, int value) in processor.TasksForUserByProject(buffer))
            {
                Console.WriteLine($"{key.Name} : {value}");
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Second request: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Enter value of UserId");
            Console.ForegroundColor = default;
            buffer = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(JsonConvert.SerializeObject(processor.TasksByUserWithShortNames(buffer),
                Formatting.Indented));
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Third request: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Enter value of UserId");
            buffer = Convert.ToInt32(Console.ReadLine());
            Console.ForegroundColor = default;
            Console.WriteLine(JsonConvert.SerializeObject(processor.TasksFinishedInCurrentYear(buffer),
                Formatting.Indented));
            Console.WriteLine("Fourth request: ");
            Console.ForegroundColor = default;
            Console.WriteLine(JsonConvert.SerializeObject(processor.TeamInfos(),
                Formatting.Indented));
            Console.WriteLine("Fifth request: ");
            Console.ForegroundColor = default;
            Console.WriteLine(JsonConvert.SerializeObject(processor.SortedUsers(),
                Formatting.Indented));
            Console.WriteLine("Sixth request: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Enter value of UserId");
            Console.ForegroundColor = default;
            buffer = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(JsonConvert.SerializeObject(processor.UserInfo(buffer),
                Formatting.Indented));
            Console.WriteLine("Seventh request: ");
            Console.ForegroundColor = default;
            Console.WriteLine(JsonConvert.SerializeObject(processor.ProjectInfos(),
                Formatting.Indented));
        }
    }
}