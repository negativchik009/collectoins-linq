﻿namespace BSA_N_1.Models
{
    public enum Route : byte
    {
        AllProjects,
        AllTasks,
        AllTeams,
        AllUsers,
    }
}