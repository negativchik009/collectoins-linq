﻿using System;
using Newtonsoft.Json;

namespace BSA_N_1.Models
{
    public class Task
    {
        public int Id { get; set; }
        [JsonIgnore]
        public Project Project { get; set; }
        [JsonIgnore]
        public User Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}