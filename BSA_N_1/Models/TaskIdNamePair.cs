﻿namespace BSA_N_1.Models
{
    public class TaskIdNamePair
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}