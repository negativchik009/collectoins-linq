﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BSA_N_1.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
        public IEnumerable<User> Members { get; set; }
        [JsonIgnore]
        public IEnumerable<Project> Projects { get; set; }
    }
}