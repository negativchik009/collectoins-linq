﻿using System.Collections.Generic;

namespace BSA_N_1.Models
{
    public class TeamInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<User> Members { get; set; }
    }
}