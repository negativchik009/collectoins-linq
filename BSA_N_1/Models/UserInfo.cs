﻿using System.Linq;

namespace BSA_N_1.Models
{
    public class UserInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }

        public int LastProjectTasksNumber { get; set; }

        public int UnfinishedTasksNumber { get; set; }

        public Task LongestTask { get; set; }
    }
}