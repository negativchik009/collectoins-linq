﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BSA_N_1.Models
{
    public class User
    {
        public int Id { get; set; }
        [JsonIgnore]
        public Team Team { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime? RegisteredAt { get; set; }
        public DateTime? BirthDay { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
        [JsonIgnore]
        public IEnumerable<Project> Projects { get; set; }
    }
}