﻿using System.Collections.Generic;
using BSA_N_1.Models;

namespace BSA_N_1.Interfaces
{
    public interface IDataGetter
    {
        public IEnumerable<Project> GetData();
    }
}