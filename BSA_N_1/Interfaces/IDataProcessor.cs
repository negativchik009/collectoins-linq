﻿using System;
using System.Collections;
using System.Collections.Generic;
using BSA_N_1.Models;

namespace BSA_N_1.Interfaces
{
    public interface IDataProcessor
    {
        public Dictionary<Project, int> TasksForUserByProject(int userId); // selection 1
        public IEnumerable<Task> TasksByUserWithShortNames(int userId); // selection 2
        public IEnumerable<TaskIdNamePair> TasksFinishedInCurrentYear(int userId); // selection 3
        public IEnumerable<TeamInfo> TeamInfos(); // selection 4
        public IEnumerable<User> SortedUsers(); // selection 5
        public UserInfo UserInfo(int userId); // selection 6
        public IEnumerable<ProjectInfo> ProjectInfos(); // selection 7
    }
}