﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using BSA_N_1.Interfaces;
using BSA_N_1.Models;

namespace BSA_N_1
{
    public class DataProcessor : IDataProcessor
    {
        private IEnumerable<Project> _data;

        public IEnumerable<Project> Data
        {
            set => _data = value;
        }

        public DataProcessor(IEnumerable<Project> projects)
        {
            _data = projects;
        }

        public Dictionary<Project, int> TasksForUserByProject(int userId)
        {
            return new Dictionary<Project, int>(_data.Where(project => project.Tasks != null && project.Tasks.Any(task => task.Performer.Id == userId)).Select(project => new KeyValuePair<Project, int>(
                project, project.Tasks.Count(task => task.Performer.Id == userId)))
            );
        }

        public IEnumerable<Task> TasksByUserWithShortNames(int userId)
        {
            const int maxLenght = 45;
            return _data.Aggregate(new List<Task>() as IEnumerable<Task>,
                (list, project) =>
                    list.Union(project.Tasks.Where(task =>
                        task.Name.Length < maxLenght && task.Performer.Id == userId)))
                .ToList();
        }

        public IEnumerable<TaskIdNamePair> TasksFinishedInCurrentYear(int userId)
        {
            return _data.SelectMany(project => project.Tasks.Where(task => 
                        task.FinishedAt?.Year == DateTime.Now.Year && task.Performer.Id == userId),
                (project, task) => new TaskIdNamePair()
                {
                    Id = task.Id,
                    Name = task.Name
                })
                .ToList();
        }

        public IEnumerable<TeamInfo> TeamInfos()
        {
            const int minAge = 10;
            return _data.Select(project => project.Team)
                .Distinct()
                .Where(team => team.Members.All(user => 
                    DateTime.Now.Year - user.BirthDay?.Year > minAge))
                .Select(team => new TeamInfo()
                {
                    Id = team.Id,
                    Name = team.Name,
                    Members = team.Members.OrderByDescending(x => x.BirthDay)
                })
                .ToList();
        }

        public IEnumerable<User> SortedUsers()
        {
            return _data.SelectMany(project => project.Team.Members)
                .Distinct()
                .Select(member => new User()
                {
                    Id = member.Id,
                    Email = member.Email,
                    BirthDay = member.BirthDay,
                    FirstName = member.FirstName,
                    LastName = member.LastName,
                    Projects = member.Projects,
                    RegisteredAt = member.RegisteredAt,
                    Tasks = member.Tasks.OrderByDescending(x => x.Name.Length),
                    Team = member.Team
                })
                .OrderBy(x => x.FirstName);
        }

        public UserInfo UserInfo(int userId)
        {
            return _data.Select(project => project.Tasks.Where(task => task.Performer.Id == userId))
                .Where(tasks => tasks.Any())
                .Join(_data, 
                    tasks => userId, 
                    ownedProject => ownedProject.Author.Id, 
                    (tasks, ownedProject) =>
                    new UserInfo()
                    {
                        User = ownedProject.Author,
                        LastProject = ownedProject,
                        LongestTask = tasks.Aggregate((longest, current) => (current.FinishedAt ?? DateTime.Now) - current.CreatedAt >
                                                                            (longest.FinishedAt ?? DateTime.Now) - longest.CreatedAt ? current : longest),
                        LastProjectTasksNumber = ownedProject.Tasks.Count(),
                        UnfinishedTasksNumber = tasks.Count(task => task.State != TaskState.Done)
                    })
                .Aggregate((latest, current) => 
                    current.LastProject.CreatedAt > latest.LastProject.CreatedAt ? current : latest);
        }

        public IEnumerable<ProjectInfo> ProjectInfos()
        {
            const int descriptionMinLenght = 20;
            const int minTaskCount = 3;
            return _data.Where(project => project.Tasks.Count() > minTaskCount && project.Description.Length > descriptionMinLenght)
                .Select(project => new ProjectInfo()
            {
                Project = project,
                LongestByDescription = project.Tasks
                    .Aggregate((longest, task) =>
                        task.Description.Length > longest.Description.Length ? task : longest),
                ShortestByName = project.Tasks.Aggregate((shortest, task) =>
                    task.Name.Length < shortest.Name.Length ? task : shortest),
                TeamUsersCount = project.Team.Members.Count()
            });
        }
    }
}