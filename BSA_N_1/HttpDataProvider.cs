﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using BSA_N_1.Dto;
using BSA_N_1.Interfaces;
using BSA_N_1.Models;
using Newtonsoft.Json;

namespace BSA_N_1
{
    public class HttpDataGetter : IDataGetter
    {
        private readonly HttpClient _client;
        
        private readonly Dictionary<Route, string> _routes;

        private IEnumerable<Project> _projects;

        public HttpDataGetter(Dictionary<Route, string> routes)
        {
            _routes = routes;
            _client = new HttpClient();
            var usersDto = JsonConvert
                .DeserializeObject<IEnumerable<UserDto>>
                    (_client.GetStringAsync(_routes[Route.AllUsers]).Result);
            var projectsDto = JsonConvert
                .DeserializeObject<IEnumerable<ProjectDto>>
                    (_client.GetStringAsync(_routes[Route.AllProjects]).Result);
            var teamsDto = JsonConvert
                .DeserializeObject<IEnumerable<TeamDto>>
                    (_client.GetStringAsync(_routes[Route.AllTeams]).Result);
            var tasksDto = JsonConvert
                .DeserializeObject<IEnumerable<TaskDto>>
                    (_client.GetStringAsync(_routes[Route.AllTasks]).Result);
            IEnumerable<Team> teams = teamsDto.Select(t => new Team()
            {
                Id = t.Id,
                Name = t.Name,
                CreatedAt = t.CreatedAt,
                Members = new List<User>(),
                Projects = new List<Project>()
            }).ToList();
            IEnumerable<User> users = usersDto.Join(teams,
                    u => u.TeamId,
                    t => t.Id,
                    (u, t) =>
                    {
                        var user = new User()
                        {
                            Id = u.Id,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            Email = u.Email,
                            RegisteredAt = u.RegisteredAt,
                            BirthDay = u.BirthDay,
                            Tasks = new List<Task>(),
                            Projects = new List<Project>(),
                            Team = t
                        };
                        t.Members = t.Members.Append(user);
                        return user;
                    }).ToList();
            IEnumerable<Project> projects = projectsDto
                .Join(teams,
                    project => project.TeamId,
                    team => team.Id,
                    (project, team) => new {project, team})
                .Join(users, t => t.project.AuthorId, user => user.Id, (t, user) =>
                {
                    var result = new Project()
                    {
                        Id = t.project.Id,
                        Name = t.project.Name,
                        Description = t.project.Description,
                        Deadline = t.project.Deadline,
                        CreatedAt = t.project.CreatedAt,
                        Author = user,
                        Team = t.team,
                        Tasks = new List<Task>()
                    };
                    user.Projects = user.Projects.Append(result);
                    t.team.Projects = t.team.Projects.Append(result);
                    return result;
                }).ToList();
            IEnumerable<Task> tasks = tasksDto.Join(users,
                task => task.PerformerId,
                user => user.Id,
                (task, user) => new {task, user})
                .Join(projects,
                    t => t.task.ProjectId,
                    project => project.Id,
                    (t, project) =>
                    {
                        var result = new Task()
                        {
                            Id = t.task.Id,
                            CreatedAt = t.task.CreatedAt,
                            FinishedAt = t.task.FinishedAt,
                            Description = t.task.Description,
                            Name = t.task.Description,
                            State = t.task.State,
                            Performer = t.user,
                            Project = project
                        };
                        t.user.Tasks = t.user.Tasks.Append(result);
                        project.Tasks = project.Tasks.Append(result);
                        return result;
                    }).ToList();
            _projects = projects;
        }
        public IEnumerable<Project> GetData()
        {
            return _projects;
        }
    }
}