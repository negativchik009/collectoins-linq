﻿using System;

namespace BSA_N_1.Dto
{
    internal class TeamDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}